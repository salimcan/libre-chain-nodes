#!/bin/bash

################################################################################
# 
# Created by http://CryptoLions.io
#
###############################################################################
source /opt/libre-chain-nodes/libreNode/node.env
$WALLETDIR/stop_wallet.sh
$NODEOSBINDIR/keosd --config-dir $DATADIR --wallet-dir $DATADIR --unix-socket-path $DATADIR/keosd.sock --http-server-address $WALLETHOST:$WALLETPORT "$@" > $DATADIR/stdout.txt 2> $DATADIR/stderr.txt & echo $! > $DATADIR/wallet.pid

