#!/bin/bash
source /opt/libre-chain-nodes/libreNode/node.env

set -e
# variables - these can be added to the node.env
ACCT="account" #replace with your account name
WALLETNAME="wallet" #replace with your wallet name
PASS="PW44747pass" #replace with your wallet password
DAYS=`echo $(( $RANDOM % 365 + 180 ))` #days to stake for

# run 
cleos wallet unlock -n $WALLETNAME --password $PASS > /dev/null 2>&1 || true
echo "xxx claim $BLOCKCHAIN $NETWORK xxx"
cd $MAINDIR
echo "Claim: $ACCT"
./cleos.sh system claimrewards $ACCT -p $ACCT@active
sleep 2
BALANCE=`./cleos.sh get currency balance eosio.token $ACCT`
./cleos.sh transfer $ACCT stake.libre "$BALANCE" "stakefor:$DAYS" -p $ACCT@active
./cleos.sh push action eosio voteproducer '{"voter": "'$ACCT'", "producers": "'$ACCT'"}' -p $ACCT@active
