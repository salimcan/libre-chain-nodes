#!/bin/bash
# EXPERIMENTAL - TESTED ONLY TWICE
source /opt/libre-chain-nodes/libreNode/node.env

sudo apt update
sudo apt install git jq libtinfo5
sudo chown `whoami` /opt
mkdir -p /opt/libre/src
cd /opt/libre/src/
# setup node script for Ubuntu 18.04 and nodeos 3.1.0 --> replace if using Ubuntu 20.04 with the comments below
wget https://apt.eossweden.org/antelope/pool/testing/l/leap-310/leap-310_3.1.0-ubuntu-18.04_amd64.deb
# uncomment the follwing 2 lines for Ubuntu 20.04
# wget http://security.ubuntu.com/ubuntu/pool/main/i/icu/libicu60_60.2-3ubuntu3.2_amd64.deb
# sudo dpkg -i libicu60_60.2-3ubuntu3.2_amd64.deb
sudo dpkg -i leap-310_3.1.0-ubuntu-18.04_amd64.deb
cd /opt
git clone https://gitlab.com/libre-chain/libre-chain-nodes
cd /opt/libre-chain-nodes/libreNode
source node.env
ln -s /usr/opt/leap/310-mv/bin/nodeos $MAINDIR/nodeos
ln -s /usr/opt/leap/310-mv/bin/cleos $MAINDIR/cleos
ln -s /usr/opt/leap/310-mv/bin/keosd $MAINDIR/keosd
sed -i 's/nodeos/\.\/nodeos/g' $MAINDIR/start.sh
sed -i 's/nodeos/\.\/nodeos/g' $MAINDIR/State-History/history-start.sh
cp $MAINDIR/Scripts/snapstart.sh $MAINDIR/snapstart.sh
cat $MAINDIR/peers.ini >> $MAINDIR/config.ini
# uncomment for setting up state history node
#cp $MAINDIR/State-History/config.ini $MAINDIR/config.ini
#cp $MAINDIR/State-History/history-start.sh $MAINDIR
echo "type: cd /opt/libre-chain-nodes/libreNode to get to your node directory"
echo "type: ./snapstart.sh to run your node on testnet"