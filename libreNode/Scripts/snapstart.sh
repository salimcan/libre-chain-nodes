#!/bin/bash
################################################################################
# Libre tools 
# Originated from scripts by by CryptoLions.io
###############################################################################
source /opt/libre-chain-nodes/libreNode/node.env

$MAINDIR/stop.sh
echo -e "Starting Nodeos \n";

ulimit -n 65535
ulimit -s 64000

wget $SNAPSHOTLINK -O $SNAPDIR/latest.bin.tar.gz
cd snapshots
rm *.bin
tar -zxvf latest.bin.tar.gz
cd ..
$NODEOSBINDIR/nodeos --data-dir $DATADIR --config-dir $MAINDIR "$@" --snapshot `ls -Art $SNAPDIR/*.bin | tail -n 1` --delete-all-blocks > $DATADIR/stdout.txt 2> $DATADIR/stderr.txt &  echo $! > $DATADIR/nodeos.pid
echo "started nodeos proc `cat $DATADIR/nodeos.pid` log $DATADIR/stderr.txt"
