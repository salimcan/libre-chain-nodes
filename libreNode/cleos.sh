#!/bin/bash
################################################################################
# Libre tools  
###############################################################################
source /opt/libre-chain-nodes/libreNode/node.env
$NODEOSBINDIR/cleos -u http://$HTTPLISTEN:$HTTPPORT --wallet-url http://$WALLETHOST:$WALLETPORT "$@"