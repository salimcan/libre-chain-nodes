#!/bin/bash
################################################################################
# Libre tools
# To generate keys 
# Usage: ./generate_key.sh name-of-key 
################################################################################## 
source /opt/libre-chain-nodes/libreNode/node.env

$NODEOSBINDIR/cleos create key --file $1
cat $1